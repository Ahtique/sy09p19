source('kppv.val.R')
#' Tune k-mean
#' 
#' Trouve la valeur optimale de K pour un kppv.val
#'
#' @param Xapp 
#' @param zapp 
#' @param Xval 
#' @param zval 
#' @param nppv 
#'
#' @return
#' @export
#'
#' @examples
kppv.tune <- function(Xapp, zapp, Xval, zval, nppv)
{
	taux <- rep(0,length(nppv))

	for (k in nppv)
	{
    zpred <- kppv.val(Xapp, zapp, k, Xval)
    taux[k] <- mean(zval == zpred)
	}

	out <- NULL
  out$k <- which.max(taux)
  out$taux <- taux[out$k]
  out
}
