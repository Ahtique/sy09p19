#' Separe les donn�es en 2 parties
#' 
#' split en donn�es de test et donn�e d'apprentissage al�atoireent
#'
#' @param X vecteur de valeurs
#' @param z classes des �l�ments de X
#' @param ratio donn�es apprentissage
#'
#' @return out = Xapp, zapp, Xtst, ztst
#' @export
#'
#' @examples
separ1 <- function(X, z, ratio)
{
	ntot <- dim(X)[1]

	iapp <- sample(1:ntot, round(ratio*ntot))
	itst <- setdiff(1:ntot, iapp)

	Xapp <- X[iapp,]
	zapp <- z[iapp]
	Xtst <- X[itst,]
	ztst <- z[itst]

	out <- NULL
	out$Xapp <- Xapp
	out$zapp <- zapp
	out$Xtst <- Xtst
	out$ztst <- ztst

	out
}
