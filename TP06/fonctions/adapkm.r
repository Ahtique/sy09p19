adapkm <- function(
  X, # tableau de donnees � regrouper
  K=2, # nombre de classes
  rhok = rep(1,K), 
  iter.max=100, # nombre d'iterations max
  nstart=1, # nombre d'essais
  epsi=1e-5 # precision
  ){
  X <- as.matrix(X) # df => matrice n,p
  n <- dim(X)[1] # nombre de points
  p <- dim(X)[2] # nombre de variables
  #  inertie totale � Inf pour �tre s�r d maj la premi�re fois
  best.dtot <- Inf 
  # on essaie nstart fois
  for (iess in 1:nstart){
    # selection des centres initiaux
    # on choisis K points au hasard dans X
    muk <- X[sample(1:n,K),]
    # initialisation
    # matrice des covariances
    Sig <- array(rep(diag(rep(1,p)+1e-5),K), dim=c(p,p,K))
    # conditions d'arr�t
    diff <- epsi+1
    iter <- 0
    # tant que pas de convergence et iter max n'est pas d�pass�
    while ((diff>epsi)&(iter<=iter.max)){
      iter <- iter+1
      # distance � chaque muk en chaque ligne
      dMah <- matrix(0, nrow=n, ncol=K)
      for (k in 1:K){
        # mat de covariance normalis�e
        vk <- Sig[,,k]
        # calcul des distances de chaque points 
        dMah[,k] <- distXY(X, muk[k,], vk)
      }
      # pour chaque ligne r�cup�re l'index du min de la ligne
      # ie recup�re en n le cluster auuel est rattach� n
      clus <- apply(dMah, 1, which.min)
      # save des anciens muk
      mukold <- muk
      # on calcule les nouveau centres
      for (k in 1:K){
        # on recup les points du cluster k
        Xk <- X[clus == k,]
        # nouveau muk = moyenne du cluster
        muk[k,] <- colMeans(Xk)
        # nouvelle matrice des covariances Vk
        Sig[,,k] <- cov(Xk)
        # regularisation 
        # Sig[,,k] <- Sig[,,k] + diag(p)*1e-5*mean(diag(Sig[,,k]))
        # normalisation 
        Sig[,,k] <- (rhok[k] * det(Sig[,,k]))^(-1/p) * Sig[,,k]
      }
      # estimation de la convergence
      # somme des inerties intraclasse
      dtot <- 0
      for (k in 1:K){
        Xk <- X[clus == k, ]
        dtot <- dtot + sum(distXY(Xk, muk[k,], vk))
      }
      # crit�re de convergence
      # diff <- 0
      # for (k in 1:K){
      #   diff <- diff + sum(distXY(muk[k,], mukold[k,], vk))
      # }
      diff <- abs(best.dtot - dtot)
    }

    if (dtot<best.dtot){
      best.dtot <- dtot
      best.iter <- iter
      best.clus <- clus
      best.muk <- muk
      best.Sig <- Sig
    }
  }

  outp <- NULL
  outp$dtot <- best.dtot
  outp$iter <- best.iter
  outp$cluster <- best.clus
  outp$centers <- best.muk
  outp$MatCov <- best.Sig
  outp
}

# plot X given a k-mean object
plotkm <- function(X, km){
  plot(X, col=km$cluster, add=TRUE)
  points(km$centers, pch = 2)
}
